// Splides (slideshows)

let splides = document.querySelectorAll('.splide');

if (splides.length > 0) {
  for (let splide of splides) {
    let slideshow = new Splide( splide, {
      type: 'loop',
      speed: 0,
      perPage: 1,
      autoWidth: splide.classList.contains('splide--autoWidth'),
      updateOnMove: true,
      clonesStatus: false,
      clones: 0,
      pagination: false
    });
    slideshow.on('mounted', function() {
      let firstSlideWidth = Number(document.querySelector('#splide01-slide01 img').getAttribute('width'));
      slideshow.root.style = `max-width:${firstSlideWidth}px`;
      document.querySelector('.splide__arrow--prev').innerHTML = '<abbr title="Précédant">Préc.</abbr>';
      document.querySelector('.splide__arrow--next').innerHTML = '<abbr title="Suivant">Suiv.</abbr>';
    });
    slideshow.mount();

    slideshow.on('move', function(newIndex) {
      // Update slideshow width
      let activeImage  = document.querySelector('.splide__slide.is-active img');
      let activeWidth  = activeImage.offsetWidth;
      // let activeHeight = activeImage.offsetHeight;
      slideshow.root.style = `max-width:${activeWidth}px`;

      // Update index
      slideshow.root.setAttribute('data-index', newIndex+1);

      // Update caption
      let activeFigcaption = document.querySelector('.splide__slide.is-active figcaption');
      let activeCaption = activeFigcaption ? activeFigcaption.textContent : '';
      slideshow.root.setAttribute('data-caption', activeCaption);
    });
  }
}