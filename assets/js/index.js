// Logo

const logo = document.querySelector(".logo");
const header = document.querySelector(".header");

logo.addEventListener('click', function() {
  header.getAttribute('data-expanded') === 'false'
  ? header.setAttribute('data-expanded', 'true')
  : header.setAttribute('data-expanded', 'false');
})


// Search

const searchInput = document.querySelector('#input-search');
const searchSubmit = document.querySelector('#submit-search');

function toggleSearch(label) {
  // Toggle search label, input and submit [hidden] attribute
  label.toggleAttribute('hidden');
  searchInput.toggleAttribute('hidden');
  searchSubmit.toggleAttribute('hidden');
}


// Tags

const tagsButton = document.querySelector('button[aria-controls="tags"]');
const tags = document.querySelector('#tags');

function toggleTags(button) {
  // Toggle button [aria-expanded] attribute
  if (button.getAttribute('aria-expanded') === 'false') {
    button.setAttribute('aria-expanded', 'true');
  } else {
    button.setAttribute('aria-expanded', 'false')
  }
}


// Horizontal Grab & Drag Scrolling

const galleries = document.querySelectorAll('.gallery:not(.splide)');
const scrollSpeed = 1;

for (let gallery of galleries) {
  if (isOverflowing(gallery)) {
    gallery.classList.add('draggable');
  }
  let isDown = false;
  let startX;
  let scrollLeft;
  gallery.addEventListener('mousedown', (e) => {
    isDown = true;
    // gallery.classList.add('active');
    startX = e.pageX - gallery.offsetLeft;
    scrollLeft = gallery.scrollLeft;
  });
  gallery.addEventListener('mouseleave', () => {
    isDown = false;
    // gallery.classList.remove('active');
  });
  gallery.addEventListener('mouseup', () => {
    isDown = false;
    // gallery.classList.remove('active');
  });
  gallery.addEventListener('mousemove', (e) => {
    if(!isDown) return;
    e.preventDefault();
    const x = e.pageX - gallery.offsetLeft;
    const walk = (x - startX) * scrollSpeed;
    gallery.scrollLeft = scrollLeft - walk;
  });
}


// Detect if element is overflowing

function isOverflowing(el) {
  return el.scrollHeight > el.clientHeight || el.scrollWidth > el.clientWidth;
}

window.addEventListener('resize', function(event) {
  for (let gallery of galleries) {
    if (isOverflowing(gallery)) {
      gallery.classList.add('draggable');
    }
  }
}, true);


// Remove blurred image placeholder when image is loaded

const images = document.querySelectorAll('img[style^="background-image"]');
for (let img of images) {
  img.addEventListener('load', function() {
    img.removeAttribute('style');
  });
}

