Title: Marsatac 24

----

Date: 2024-04-01

----

Tags: Dessin, Identité

----

Headline: Dessins pour l'identité graphique de l'édition 2024 du Festival Marsatac.

----

Layout: [{"attrs":{"class":"","id":"","toggle":"false","summary":"","open":"false","marginbottom":""},"columns":[{"blocks":[{"content":{"class":"","id":"","text":"<p>Avec N.Guillerminet.</p>"},"id":"ed8e8b60-3888-4257-a9f7-e15a8e874a06","isHidden":false,"type":"text"}],"id":"a39fcb40-8c35-4907-975d-8e49d31f4d00","width":"1/1"}],"id":"9dfa2a14-ba33-4a05-9ecd-2b7a70c8339f"},{"attrs":{"class":"","id":"","toggle":"false","summary":"","open":"false","marginbottom":""},"columns":[{"blocks":[{"content":{"location":"kirby","image":["file://AicpaDx5HtHOVwvN"],"src":"","alt":"","caption":"","credits":"","link":"","ratio":"","crop":"false"},"id":"56288dea-9e9e-4f03-ac37-b44eec11b5a4","isHidden":false,"type":"image"}],"id":"a60878cd-3273-4432-beb2-4ce6d58648f0","width":"1/1"}],"id":"44bd327e-eb3c-4664-8685-88966c64f480"}]

----

Metatitle: 

----

Metadescription: 

----

Metarobots: 

----

Metaimage: 

----

Uuid: tzDZV9HC1lPRVx6H