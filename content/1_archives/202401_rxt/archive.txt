Title: RXT

----

Date: 2024-01-01

----

Tags: Identité

----

Headline: Logotype pour RXT, entreprise spécialisée dans la logistique de mobilier de designers.

----

Layout: [{"attrs":{"class":"","id":"","toggle":"false","summary":"","open":"false","marginbottom":""},"columns":[{"blocks":[{"content":{"location":"kirby","image":["file://70AF2E9pYfarn19w"],"src":"","alt":"","caption":"","credits":"","link":"","ratio":"","crop":"false"},"id":"cbbf348c-e132-42ca-8329-4b369fd8c86b","isHidden":false,"type":"image"}],"id":"91e60f00-0749-4cdd-a86e-5b0665d95210","width":"1/1"}],"id":"36af6489-58e3-4707-bc13-9d18315d2a2d"}]

----

Metatitle: 

----

Metadescription: 

----

Metarobots: 

----

Metaimage: 

----

Uuid: Vnap03P4JJEUA9qS