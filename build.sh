#!/bin/bash

# To execute this script, open the Terminal and run:
# chmod +x ./workflow.sh
# ./workflow.sh

# Merge and move the CSS files
cat assets/css/src/*.css > assets/css/style.css
echo "CSS files merge and move"

# Minify concatened CSS file
# look at https://stackoverflow.com/questions/13501565/regex-to-beautify-minified-css
# cat assets/css/style.css | sed -e 's/{[[:blank:]]/{/g' > assets/css/test.min.css
minify --type=css -o assets/css/style.min.css assets/css/style.css
echo "CSS minify"
