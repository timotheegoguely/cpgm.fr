<?php snippet('head') ?>

<h1 class="sr-only"><?= $page->title()->esc() ?></h1>

<div class="wrapper">
  <?= $page->text() ?>
  <h2>Actualités</h2>
  <?php snippet('newsletter') ?>
</div>

<?php snippet('layout') ?>
<?php snippet('footer') ?>