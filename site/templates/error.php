<?php snippet('head') ?>

<div class="wrapper">
  <h1><?= $page->title()->esc() ?></h1>
  <?= $page->text() ?>
</div>

<?php snippet('footer') ?>