<!DOCTYPE html>
<html lang="fr" translate="no">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Page protégée | Cpgm</title>
  <meta name="author" content="<?= e($page->author()->isNotEmpty(), $page->author(), $site->title()) ?>">
  <meta name="generator" content="Kirby (<?= Kirby::version() ?>)">
  <meta name="robots" content="noindex, nofollow">
  <!-- Styles -->
  <?php if ($kirby->option('local') === true): ?>
  <!-- CSS for local dev -->
  <?= css(['assets/css/index.css?'.filemtime('assets/css/index.css'), '@auto']) ?>
  <?php else: ?>
  <?= css(['assets/css/style.min.css?'.filemtime('assets/css/style.min.css'), '@auto']) ?>
  <?php endif ?>

</head>

<body>
  <main id="main" class="wrapper">
    <form method="post">
      <label for="password" class="sr-only">Mot de passe</label>
      <input type="password" name="password" placeholder="Mot de passe" value="<?= esc(get('password', '')) ?>">
      <input type="hidden" name="csrf" value="<?= csrf() ?>">
      <button type="submit">Entrer</button>
      <?php if ($error): ?>
      <span class="disabled"><?= $error ?></span>
      <?php endif ?>
    </form>
  </main>
</body>
</html> 

