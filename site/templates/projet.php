<?php snippet('head') ?>

<header class="wrapper">
  <h1><?= $page->title()->esc() ?></h1>
</header>

<?php snippet('layout') ?>

<?php snippet('footer') ?>
