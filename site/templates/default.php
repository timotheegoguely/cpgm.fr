<?php snippet('head') ?>

<div class="wrapper">
  <h1 class="sr-only"><?= $page->title()->esc() ?></h1>
  <?= $page->text()->kt() ?>
</div>

<?php snippet('footer') ?>