<?php snippet('head') ?>

<section class="wrapper | flow">
  <h1 class="sr-only"><?= $page->title()->html() ?></h1>
  <?php foreach ($archives as $archive): ?>
  <div class="archive">
    <time datetime="<?= $archive->date() ?>"><?= $archive->date()->toDate('y.m.') ?></time>
    <details class="flow">
      <summary><h2><?= $archive->headline()->inline() ?></h2></summary>
      <?php if ($archive->text()->isNotEmpty()): ?>
      <div class="text">
        <?= $archive->text() ?>
      </div>
      <?php endif ?>
      <?php if ($archive->layout()->isNotEmpty()): ?>
      <?= snippet('layout', ['layout' => $archive->layout(), 'template' => 'archives']) ?> 
      <?php endif ?>
    </details>
  </div>
  <?php endforeach ?>
</section>

<?php snippet('footer') ?>