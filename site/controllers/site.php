<?php

return function ($page, $pages, $site, $kirby) {
   
  $template = $page->template();
  
  // Variables for Meta tags
  $title = $page->isHomePage() 
  ? $site->metaTitle()->or($site->title()) 
  : $page->metaTitle()->or($page->title())." | ".$site->metaTitle()->or($site->title());
  $description = $page->isHomePage() 
  ? $site->metaDescription()->or($site->homePage()->metaDescription())
  : $page->metaDescription()->or($site->metaDescription());
  $canonical = $page->canonical()->or($page->url());

  // Get image for social sharing, else default site image, else null
  if ($page->metaImage()->toFile() !== null) {
    $metaImage = $page->metaImage()->toFile();
  } elseif ($site->metaImage()->toFile() !== null) {
    $metaImage = $site->metaImage()->toFile();
  } else {
    $metaImage = null;
  }

  $slideshow = false;
  if ($template == "archives") {
    foreach ($kirby->collection("archives") as $archive) {
      if ($archive->layout()->isNotEmpty() && !$slideshow) {
        if (Str::contains($archive->layout(), '"slideshow":"true"')) {
          $slideshow = true;
        }
      }
    }
  } elseif ($template == "projet") {
    $slideshow = true;
  }

  // Search
  $query = get('q');

  return compact('template', 'title', 'description', 'canonical', 'metaImage', 'query', 'slideshow');

};