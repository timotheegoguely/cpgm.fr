<?php

return function ($page, $pages, $site, $kirby) {

	// Grab the data from the default controller
	$shared = $kirby->controller('site', compact('page', 'pages', 'site', 'kirby'));

	// Fetch all listed projets
	$projets = $page->children()->listed()->sortBy('date', 'desc');

	// Fetch all tags
	$tags = $projets->pluck('tags', ',', true);

	// Add the tag filter
	if($tag = param('tag')) {
	  $projets = $projets->filterBy('tags', $tag, ',');
	}

	// Return the array containing the data that we want to pass to the template
	return a::merge($shared , compact(
		'shared',
		'projets',
		'tags'
	));

};
