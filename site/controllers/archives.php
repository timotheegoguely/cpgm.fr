<?php

return function ($page, $pages, $site, $kirby) {

	// Grab the data from the default controller
	$shared = $kirby->controller('site', compact('page', 'pages', 'site', 'kirby'));

	// Fetch all listed archives
	$archives = $page->children()->listed()->sortBy('date', 'desc');

	// Fetch all tags
	$tags = $archives->pluck('tags', ',', true);

	// Add the tag filter
	if($tag = param('tag')) {
	  $archives = $archives->filterBy('tags', $tag, ',');
	}

	// Search
	$query = get('q');
	if ($query) {
		$archives = $site->search($query, 'title|headline|description|tags');
	}

	// Return the array containing the data that we want to pass to the template
	return a::merge($shared , compact(
		'shared',
		'archives',
		'query',
		'tags'
	));

};
