<?php

return function ($page, $pages, $site, $kirby) {

	// Fetch data from the default controller
	$shared = $kirby->controller('site', compact('page', 'pages', 'site', 'kirby'));

	// Return the array containing the data that we want to pass to the template
	return a::merge($shared , compact(
		'shared'
	));

};
