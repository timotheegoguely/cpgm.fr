</main>

<?php if ($slideshow): ?>
<?= js('assets/js/dist/splide.min.js', ['defer' => true]) ?>
<?php endif ?>
<?php if ($kirby->option('local') === true): ?>
<?= js(['assets/js/index.js?'.filemtime('assets/js/index.js'), '@auto'], ['defer' => true]) ?>
<?php if ($slideshow): ?>
<?= js('assets/js/slideshows.js', ['defer' => true]) ?>
<?php endif ?>
<?php else: ?>
<?= js(['assets/js/index.min.js?'.filemtime('assets/js/index.min.js'), '@auto'], ['defer' => true]) ?>
<?php if ($slideshow): ?>
<?= js('assets/js/slideshows.min.js', ['defer' => true]) ?>
<?php endif ?>
<?php endif ?>

</body>
</html> 
