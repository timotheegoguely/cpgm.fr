<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "name": "<?= $site->title() ?>",
  "url": "<?= $site->url() ?>",
  "description": "<?= $site->metaDescription() ?? $site->description() ?>",
  "inLanguage": "fr"
}
</script>
