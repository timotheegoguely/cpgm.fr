<?php 
$layout = $layout ?? $page->layout();
$template = $template ?? $template->layout();
?>
<?php foreach ($layout->toLayouts() as $layout): ?>
<?php 
$toggle  = $layout->attrs()?->toggle()->isTrue();
$summary = $layout->attrs()?->summary();
$open    = $layout->attrs()?->open()->isTrue();
?>

<?php if ($template === 'archives'): ?>

<?php foreach ($layout->columns() as $column): ?>
  <?php if ($toggle): ?>
  <details class="flow"<?= e($open, ' open') ?>>
    <summary><?= $summary ?></summary>
  <?php endif ?>
    <?php foreach ($column->blocks() as $block): ?>
    <?= $block ?>
    <?php endforeach ?>
  <?php if ($toggle): ?>
  </details>
  <?php endif ?>
<?php endforeach ?>

<?php else: ?>

<section class="wrapper"<?= e($layout->marginBottom()->isNotEmpty(), ' style="margin-bottom: var(--space-'.$layout->marginBottom().')"' ) ?>>
  <?php if ($toggle): ?>
  <details<?= e($open, ' open') ?>>
    <summary><h2><?= $summary ?></h2></summary>
  <?php endif ?>
  <div class="layout | grid">
    <?php foreach ($layout->columns() as $column): ?>
    <div class="column" style="--columns:<?= $column->span() ?>">
      <div class="blocks | flow">
        <?php foreach ($column->blocks() as $block): ?>
        <div class="block<?= e($block->class()->isNotEmpty(), ' | '.$block->class()) ?>"<?= e($block->toggle()->isTrue(), ' data-toggle') ?>>
        <?= $block ?>
        </div>
        <?php endforeach ?>
      </div>
    </div>
    <?php endforeach ?>
  </div>
  <?php if ($toggle): ?>
  </details>
  <?php endif ?>
</section>

<?php endif ?>

<?php endforeach ?>
