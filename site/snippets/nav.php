<nav id="nav">
	<?php if($site->menu()->isNotEmpty()): ?>
	<ul id="menu" class="text-border" aria-label="Menu">
		<?php foreach($site->menu()->toStructure() as $menu): ?>
		<?php 
		$item = $menu->item();
		$isPage = $item->toPage() !== null;
		$isActive = $isPage ? $item->toPage()->isOpen() : false;
		?>
		<li<?php e($isActive, ' class="active"') ?>>
			<?php if ($page = $item->toPage()): ?>						
			<a href="<?= $page->url(); ?>"<?php e($isActive, ' aria-current="page"') ?>><?= $page->title() ?></a>
			<?php elseif (Str::startsWith($item, '#')): ?>
			<a href="<?= $item ?>"><?= Str::ucfirst(Str::replace($item, '#', '')) ?></a>
			<?php endif ?>
		</li>
		<?php endforeach ?>
	</ul>
	<?php endif ?>
</nav>
