<?php
/** @var \Kirby\Cms\Block $block */
$images         = $block->images()->toFiles();
$displayCount   = $block->displayCount()->isTrue();
$count          = $images->count();
$displayCaption = $block->displayCaption()->isTrue();
$caption        = $block->caption();
$slideshow      = $block->slideshow()->isTrue();
$crop           = $block->crop()->isTrue();
$ratio          = $block->ratio()->or('auto');
$autoWidth      = $block->autoWidth()->isTrue();
$toggle         = $block->toggle()->isTrue();
$summary        = $block->summary();
$open           = $block->open()->isTrue();
?>

<?php if ($toggle): ?>
<details<?= e($open, ' open') ?>>
  <summary><h2><?= $summary ?></h2></summary>
<?php endif ?>

<?php if ($slideshow): ?>

<div class="splide<?= e($autoWidth, ' splide--autoWidth') ?>" role="group" <?= Html::attr(['data-ratio' => $ratio, 'data-crop' => $crop, 'data-index' => '1', 'data-count' => $images->count(), 'data-caption' => $images->first()->caption()], null, ' ') ?>>
  <div class="splide__track">
    <ul class="splide__list">
      <?php foreach ($images as $image): ?>
      <?php
      $width = $image->w()->or($image->width())->toInt();
      $height = $image->h()->or($image->height())->toInt();
      $ratio = $image->height() / $image->width();
      if ($image->w()->isNotEmpty() && $image->h()->isEmpty()) {
        $height = round($image->w()->toInt() * $ratio);
      }
      elseif ($image->w()->isEmpty() && $image->h()->isNotEmpty()) {
        $width = round($image->h()->toInt() / $ratio);
      }
      ?>
      <li class="splide__slide">
        <figure>
          <img 
            draggable="false"
            src="<?= $image->url() ?>"
            <?php if (!str_ends_with($image->url(), '.svg') && !str_ends_with($image->url(), '.gif')): ?>       
            srcset="<?= $image->thumb(['width' => $width * 1, 'height' => $height * 1, 'quality' => 80])->url() ?> <?= $width * 1 ?>w,
                    <?= $image->thumb(['width' => $width * 2, 'height' => $height * 2, 'quality' => 80])->url() ?> <?= $width * 2 ?>w"
            sizes="<?= $width ?>px"
            <?php endif ?>
            alt="<?= $image->alt() ?>" 
            width="<?= $width ?>" 
            height="<?= $height ?>"
            loading="lazy"
          >
          <?php if ($image->caption()->isNotEmpty()): ?>
          <figcaption><?= $image->caption()->kti() ?></figcaption>
          <?php endif ?>
        </figure>
      </li>
      <?php endforeach ?>
    </ul>
    <?php if ($caption->isNotEmpty()): ?>
    <figcaption>
      <?= $caption ?>
    </figcaption>
    <?php endif ?>
  </div>
</div>

<?php else: ?>

<div class="gallery" role="group" <?= Html::attr(['data-ratio' => $ratio, 'data-crop' => $crop], null, ' ') ?>>
  <?php foreach ($images as $image): ?>
  <?php
  $width = $image->w()->or($image->width())->toInt();
  $height = $image->h()->or($image->height())->toInt();
  $ratio = $image->height() / $image->width();
  if ($image->w()->isNotEmpty() && $image->h()->isEmpty()) {
    $height = round($image->w()->toInt() * $ratio);
  }
  elseif ($image->w()->isEmpty() && $image->h()->isNotEmpty()) {
    $width = round($image->h()->toInt() / $ratio);
  }
  ?>
  <figure>
    <img 
      style="background-image: url('<?= $image->thUri() ?>')"
      draggable="false" 
      src="<?= $image->url() ?>"
      <?php if (!str_ends_with($image->url(), '.svg') && !str_ends_with($image->url(), '.gif')): ?>       
      srcset="<?= $image->thumb(['width' => $width * 1, 'height' => $height * 1, 'quality' => 80])->url() ?> <?= $width * 1 ?>w,
              <?= $image->thumb(['width' => $width * 2, 'height' => $height * 2, 'quality' => 80])->url() ?> <?= $width * 2 ?>w"
      sizes="<?= $width ?>px"
      <?php endif ?>
      alt="<?= $image->alt() ?>" 
      width="<?= $width ?>" 
      height="<?= $height ?>" 
      loading="lazy">
    <?php if ($displayCaption || $displayCount): ?>
    <figcaption <?= Html::attr(['data-count' => $count], null, ' ') ?>><?= e($displayCaption && $image->caption()->isNotEmpty(), $image->caption()) ?></figcaption>
    <?php endif ?>
  </figure>
  <?php endforeach ?>
</div>

<?php if ($toggle): ?>
</details>
<?php endif ?>

<?php endif ?>
