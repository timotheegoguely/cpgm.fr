<?php

/** @var \Kirby\Cms\Block $block */
$alt     = $block->alt();
$caption = $block->caption();
$crop    = $block->crop()->isTrue();
$link    = $block->link();
$ratio   = $block->ratio()->or('auto');
$src     = null;
$width   = null;
$height  = null;

if ($block->location() == 'web') {
  $src = $block->src()->esc();
} elseif ($image = $block->image()->toFile()) {
  $alt = $alt->or($image->alt());
  $src = $image->url();
  $width = $image->w()->or($image->width())->toInt();
  $height = $image->h()->or($image->height())->toInt();
  $ratio = $image->height() / $image->width();
  if ($image->w()->isNotEmpty() && $image->h()->isEmpty()) {
    $height = round($image->w()->toInt() * $ratio);
  }
  elseif ($image->w()->isEmpty() && $image->h()->isNotEmpty()) {
    $width = round($image->h()->toInt() / $ratio);
  }
}

?>
<?php if ($src): ?>
<figure <?= Html::attr(['data-ratio' => $ratio, 'data-crop' => $crop], null, ' ') ?>>
  <?php if ($link->isNotEmpty()): ?>
  <a href="<?= Str::esc($link->toUrl()) ?>">
    <img 
      src="<?= $src ?>"
      <?php if (!str_ends_with($src, '.svg') && !str_ends_with($src, '.gif')): ?>        
      srcset="<?= $image->thumb(['width' => $width * 1, 'height' => $height * 1, 'quality' => 80])->url() ?> <?= $width * 1 ?>w,
              <?= $image->thumb(['width' => $width * 2, 'height' => $height * 2, 'quality' => 80])->url() ?> <?= $width * 2 ?>w"
      sizes="<?= $width ?>px"
      <?php endif ?>
      alt="<?= $alt->esc() ?>"
      width="<?= $width ?>"
      height="<?= $height ?>"
      loading="lazy">
  </a>
  <?php else: ?>
  <img src="<?= $src ?>" alt="<?= $alt->esc() ?>" <?= Html::attr(['width' => $width, 'height' => $height], null, ' ') ?> loading="lazy">
  <?php endif ?>
  <?php if ($caption->isNotEmpty()): ?>
  <figcaption>
    <?= $caption ?>
  </figcaption>
  <?php endif ?>
</figure>
<?php endif ?>