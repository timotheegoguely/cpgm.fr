<!DOCTYPE html>
<html lang="fr" translate="no">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title><?= $title ?></title>
  <meta name="description" content="<?= $description ?>">

  <meta name="author" content="<?= e($page->author()->isNotEmpty(), $page->author(), $site->title()) ?>">
  <link rel="canonical" href="<?= $canonical ?>">
  <meta name="generator" content="Kirby (<?= Kirby::version() ?>)">
  <meta name="robots" content="<?= e($page->metaRobot()->isNotEmpty(), $page->metaRobot(), 'index follow') ?>">

  <?php snippet('favicons') ?>
  
  <!-- Styles -->
  <?= css('assets/css/dist/splide.min.css') ?>
  <?php if ($kirby->option('local') === true): ?>
  <!-- CSS for local dev -->
  <?= css(['assets/css/index.css?'.filemtime('assets/css/index.css'), '@auto']) ?>
  <?php else: ?>
  <?= css(['assets/css/style.min.css?'.filemtime('assets/css/style.min.css'), '@auto']) ?>
  <?php endif ?>
  <?= css('assets/css/print.css', ['media' => 'print']) ?>
  <?php if ($page->colorText()->isNotEmpty() || $page->colorBackground()->isNotEmpty()): ?>
  <style>
    :root {
      <?php if ($page->colorText()->isNotEmpty()): ?>
      --color-text: <?= $page->colorText() ?>;
      <?php endif ?>
      <?php if ($page->colorBackground()->isNotEmpty()): ?>
      --color-background: <?= $page->colorBackground() ?>;
      <?php endif ?>
      <?php if ($page->colorLinkHover()->isNotEmpty()): ?>
      --color-link-hover: <?= $page->colorLinkHover()->or('#00e') ?>;
      <?php endif ?>
    }
  </style>
  <?php endif ?>

  <!-- Open Graph -->
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?= $title ?>">
  <meta property="og:description" content="<?= $description ?>">
  <meta property="og:url" content="<?= $page->url() ?>">
  <?php if($metaImage !== null): ?>
  <meta property="og:image" content="<?= $metaImage->url() ?>">
  <?php elseif($site->metaImage()->toFile() !== null): ?>
  <meta property="og:image" content="<?= $site->metaImage()->toFile()->url() ?>">
  <?php endif ?>

  <?php snippet('schema/website') ?>
  <?php snippet('schema/breadcrumb') ?>

  <!-- Analytics -->
  <script defer data-domain="cpgm.fr" src="https://plausible.io/js/script.js"></script>

</head>

<body id="<?= $page->uid() ?>" data-template="<?= $template ?>">

<?php snippet('header') ?>

<main id="main">