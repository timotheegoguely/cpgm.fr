<header id="top" class="header | wrapper" data-expanded="<?= e(param('tag') || $query, 'true', 'false') ?>">
	
	<a rel="nofollow" href="#main" class="skip-link"><?= $site->skipLink()->or("Aller au contenu") ?></a>
	<span class="logo"><?= $site->title()->html() ?></span>

	<?= snippet('nav') ?>

	<?php if ($page->template()->name() === 'archives'): ?>
	<div>
		<button aria-expanded="<?= e(param('tag'), 'true', 'false') ?>" aria-controls="tags" onclick="toggleTags(this)"><span><?= e(param('tag'), param('tag'), 'Sans Filtre') ?></span></button>
		<form id="form-search">
			<label for="input-search" onclick="toggleSearch(this)"<?= e($query, ' class="underline"') ?>><?= e($query, html($query), 'Rechercher'); e($query && $archives->count() === 0, ' : aucun résultat') ?></label>
			<input type="search" name="q" id="input-search" placeholder="Rechercher" value="<?= html($query) ?>" autofocus hidden>
			<button type="submit" id="submit-search" hidden>Ok</button>
			<?php if ($query): ?>
			<a id="reset-search" href="<?= $site->homePage()->url() ?>">Réinitialiser</a>
			<?php endif ?>
		</form>
		<ul id="tags" aria-label="Tags">
			<?php foreach(collection('archives')->pluck('tags', ',', true) as $tag): ?>
			<li class="tag<?= e(param('tag') === $tag, ' active') ?>"><a href="<?= e(param('tag') === $tag, $site->homePage()->url(), url(null, ['params' => ['tag' => $tag]])) ?>"<?= e(param('tag') === $tag, ' aria-current="page"') ?>><?= html($tag) ?></a></li>
			<?php endforeach ?>
			<?php if (param('tag')): ?>
			<li id="tags-reset"><a class="text-grey" href="<?= $site->homePage()->url() ?>">Réinitialiser</a></li>	
			<?php endif ?>
		</ul>
	</div>
	<?php endif ?>

</header>
