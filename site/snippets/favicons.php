	<!-- Favicons -->
	<link rel="icon" href="/favicon.ico" sizes="32x32">
	<link rel="icon" href="/assets/favicons/favicon.svg" type="image/svg+xml">
	<link rel="apple-touch-icon" href="/assets/favicons/apple-touch-icon.png">
	<link rel="manifest" href="/manifest.webmanifest">
