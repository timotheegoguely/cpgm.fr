<div id="mlb2-16193205" class="ml-form-embedContainer ml-subscribe-form ml-subscribe-form-16193205">
  <form class="ml-block-form" action="https://assets.mailerlite.com/jsonp/1008056/forms/126032024256382292/subscribe" data-code="" method="post" target="_blank">
    <div class="ml-field-group ml-field-email ml-validate-email ml-validate-required">
      <label class="sr-only" for="input-newsletter">Adresse électronique</label>
      <input type="email" class="form-control" data-inputmask="" name="fields[email]" placeholder="Adresse électronique" autocomplete="email" id="input-newsletter">
    </div>
    <button type="submit" class="primary" id="submit-newsletter">S’abonner</button>
    <input type="hidden" name="ml-submit" value="1">
    <input type="hidden" name="anticsrf" value="true">
    <span id="confirmation" class="text-grey" aria-live="polite"></span>
  </form>
</div>
<script defer>
  function ml_webform_success_16193205() {
    const confirmation = document.querySelector('#confirmation');
    confirmation.innerText = "Abonnement pris en compte";
    setTimeout( function() { confirmation.innerText = "" }, 5000);
  }
</script>
<script defer src="https://groot.mailerlite.com/js/w/webforms.min.js?v2d8fb22bb5b3677f161552cd9e774127" type="text/javascript"></script>
<script defer>fetch("https://assets.mailerlite.com/jsonp/1008056/forms/126032024256382292/takel")</script>