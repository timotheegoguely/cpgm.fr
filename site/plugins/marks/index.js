window.panel.plugin("custom/marks", {
  writerMarks: {
    small: {
      get button() {
        return {
          icon: "small",
          label: "Small"
        }
      },

      commands() {
        return () => this.toggle()
      },

      get name() {
        return "small"
      },

      get schema() {
        return {
          parseDOM: [{ tag: "small" }],
          toDOM: () => ["small", 0]
        }
      }
    }
  }
});