<?php
Kirby::plugin('custom/blocks', [
  'blueprints' => [
    'blocks/accordion' => __DIR__ . '/blueprints/blocks/accordion.yml',
    // more blueprints
  ],
  'snippets' => [
    'blocks/accordion' => __DIR__ . '/snippets/blocks/accordion.php',
    // more snippets
  ],
  'translations' => [
    'en' => [
      'field.blocks.accordion.name' => 'Accordion',
      // more block names
    ],
    'fr' => [
       'field.blocks.accordion.name' => 'Bloc à déployer',
      // more block names
    ]
  ]
]);