<?php if($block->summary()->isNotEmpty()): ?>
  <details>
    <summary><?= $block->summary() ?></summary>
    <div><?= $block->details() ?></div>
  </details>
<?php endif ?>