panel.plugin("custom/blocks", {
  blocks: {
    accordion: {
      computed: {
        summaryField() {
          return this.field("summary");
        },
        detailsField() {
          return this.field("details");
        }
      },
      template: `
        <div @dblclick="open">
          <details>
            <summary>
              <k-writer
                ref="summary"
                :inline="true"
                marks="false"
                :placeholder="summaryField.placeholder || 'Ajouter un titre…'"
                :value="content.summary"
                @input="update({ summary: $event })"
              />
            </summary>
            <k-writer
                ref="details"
                :inline="detailsField.inline || false"
                :marks="detailsField.marks"
                :value="content.details"
                :placeholder="detailsField.placeholder || 'Ajouter le contenu'"
                @input="update({ details: $event })"
              />
          </details>
        </div>
      `
    },
  }
});