<?php

Kirby::plugin('timotheegoguely/analytics', [
	'areas' => [
		'analytics' => function ($kirby) {
			return [
				'label' => 'Analytics',
				'icon'  => 'chart',
				'menu'  => true,
				'link'  => 'https://plausible.io/share/cpgm.fr?auth=c18SXystMkvz3gUrXLT3d&embed=true&theme=light',
				// 'views' => [
				// 	[
				// 		// the Panel patterns must not start with 'panel/',
				// 		// the `panel` slug is automatically prepended.
				// 		'pattern' => 'analytics',
				// 		'action'  => function () {

				// 			return [
				// 				// the Vue component can be defined in the
				// 				// `index.js` of your plugin
				// 				'component' => 'PlausibleAnalytics',

				// 				// the document title for the current view
				// 				'title' => 'Plausible Analytics',

				// 				// props are directly available in the Vue components
				// 				// play around with setting the props to something else, e.g. use cardlets instead of cards etc.
				// 				'props' => [],
				// 			];
				// 		}
				// 	]
				// ]
			];
		}
	],
]);
