import PlausibleAnalytics from "./components/PlausibleAnalytics.vue";

panel.plugin("timotheegoguely/analytics", {
	components: {
		analytics: PlausibleAnalytics
	}
});
