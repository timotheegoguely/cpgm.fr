panel.plugin('custom/icons', {
  icons: {
    'small': '<line x1="12" y1="8" x2="12" y2="16" stroke="currentColor" stroke-width="2"/><line x1="8" y1="8" x2="16" y2="8" stroke="currentColor" stroke-width="2"/>'
  }
});