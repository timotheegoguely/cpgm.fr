<?php

return [

	'local' => false,
	'prod'  => true,
	'debug' => false,

	'smartypants' => true,
	'markdown' => [
		'extra' => true
	],

	'languages' => false,

	'panel' => [
		'css' => 'assets/css/panel.css'
	],

	'cache' => [
		'pages' => [
			'active' => true
		]
	],

	'home' => 'home',

	'thumbs' => [
		'srcsets' => [
			'default' => [
				'320w'  => ['width' => 320],
				'640w'  => ['width' => 640],
				'920w'  => ['width' => 920],
				'1280w' => ['width' => 1280]
			],
			'webp' => [
				'320w'  => ['width' => 320, 'format' => 'webp'],
				'640w'  => ['width' => 640, 'format' => 'webp'],
				'920w'  => ['width' => 920, 'format' => 'webp'],
				'1280w' => ['width' => 1280,'format' => 'webp']
			]
		]
	],

	'johannschopplich.locked-pages' => [
		'slug' => 'mot-de-passe',
		'title' => 'Page protégée',
		'error' => [
			'csrf' => 'Jeton CSRF incorrect',
			'password' => 'Incorrect'
		]
	]

];
