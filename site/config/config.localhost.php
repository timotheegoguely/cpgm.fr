<?php

return [

  'local' => true,
  'prod'  => false,
  'debug' => true,

  'cache' => [
    'pages' => [
      'active' => false
    ]
  ],

  'home' => 'archives'

];
