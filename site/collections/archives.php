<?php

return function ($site) {
	return $site->find('archives')->children()->listed();
};
